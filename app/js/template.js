(function (window) {
    'use strict';

    var Template = function () {
        this._pathToTemplates = './templates/';
        this._tmplExt = '.tmpl';
        this.toBuyItemTmpl = this._pathToTemplates + 'toBuyItem' + this._tmplExt;
    };

    Template.prototype.generate = function (data, tmpl) {
        var newTmpl = tmpl || this.toBuyItemTmpl;
        // load template using ajax
        return L.lib.template(newTmpl, data);
    };

    window.L = window.L || {};
    window.L.Template = Template;
})(window);