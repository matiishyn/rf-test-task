(function () {
    'use strict';
    /**
     * Initialization of Application
     * 'L' is a main object that contains application
     * @param title - Application title
     */
    var ToBuyList = function (title) {
        this.store = new L.Store(title);
        this.model = new L.Model(this.store);
        this.template = new L.Template();
        this.view = new L.View(this.template);
        this.controller = new L.Controller(this.model, this.view);
        this.controller.init();
    };

    var toBuyList1 = new ToBuyList('toBuyList');
})();