(function () {
    'use strict';

    var View = function (template) {
        this.$list = L.lib.qs('.to-buy-list');
        this.$addForm = L.lib.qs('.list-controllers');
        this.$addField = L.lib.qs('input', this.$addForm);
        this.$addBtn = L.lib.qs('button', this.$addForm);
        this.$items = L.lib.qsa('.to-buy-item', this.$list);
        this.$counter = L.lib.qs('.list-counter');
        this.template = template;
    };
    /**
     * add events to 'Add' button
     */
    View.prototype.addHandlers = function (target, type, callback) {
        var targetObj;
        switch (target) {
            case 'addButton':
                targetObj = this.$addBtn;
                break;
            default:
                console.error('Error in View:adHandlers');
        }
        L.lib.addEvent(targetObj, type, callback, true);
    };
    /**
     * Add new item to DOM
     * @param title
     */
    View.prototype.addItem = function (title, id) {
        var newItem = this.template.generate({
            id: id,
            title: title
        });
        //this.$list.innerHTML += newItem;
        this._renderItem(newItem);
        this._clearField();
    };

    View.prototype._renderItem = function (item) {
        this.$list.innerHTML += item;
    };

    View.prototype._clearField = function () {
        this.$addField.value = '';
    };
    /**
     * Update counter in DOM
     */
    View.prototype.updateCounter = function (number) {
        this.$counter.innerHTML = number;
    };

    /**
     * Render all items stored in LS
     * @param itemsArr
     */
    View.prototype.renderList = function (itemsArr) {
        var that = this;
        itemsArr.forEach(function (val) {
            var item = that.template.generate({
                id: val.id,
                title: val.title
            });
            that._renderItem(item);
        });
    };
    /**
     * remove element from DOM
     * @param domEl
     */
    View.prototype.removeEl = function (domEl) {
        domEl.parentElement.removeChild(domEl);
    };

    window.L = window.L || {};
    window.L.View = View;
})(window);