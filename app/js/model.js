(function (window) {
    'use strict';

    /**
     * Creation of model which will work with Store
     */
    var Model = function (store) {
        this.store = store;
    };

    Model.prototype.addItem = function (title) {
        return this.store.addItem(title);
    };

    Model.prototype.getAllItems = function () {
        return this.store.getAllItems();
    };

    Model.prototype.removeItem = function (id) {
        this.store.removeItem(id);
    };

    Model.prototype.editItem = function () {

    };

    Model.prototype.getId = function () {
        return this.store.getId();
    };
    /**
     * returns number of all items from store
     */
    Model.prototype.getAllItemsNumber = function () {
        return this.store.allItemsNumber;
    };

    window.L = window.L || {};
    window.L.Model = Model;
})(window);