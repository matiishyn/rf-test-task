/* jshint expr:true */
(function (window) {
    'use strict';

    var lib = {
        // SELECTORS
        i: function (id, scope) {
            return (scope || document).getElementById(id);
        },
        qs: function (selector, scope) {
            return (scope || document).querySelector(selector);
        },
        qsa: function (selector, scope) {
            return (scope || document).querySelectorAll(selector);
        },

        // TEMPLATE ENGINE
        template: function (tmplFileName, data) {
            var that = this,
                html = '';
            this.ajax({
                url: tmplFileName,
                async: false
            }, function (resp) {
                if (Array.isArray(data)) {
                    data.forEach(function (val) {
                        html += that._replaceInTmpl(resp, val);
                    });
                } else {
                    html += that._replaceInTmpl(resp, data);
                }
            });
            return html;
        },

        _replaceInTmpl: function (tmpl, data) {
            var html = tmpl;
            for (var prop in data) {
                if (data.hasOwnProperty(prop)) {
                    if (typeof data[prop] === 'object') {
                        html = this._replaceInTmpl(html, data[prop]);
                    } else {
                        html = html.replace(new RegExp('{{' + prop + '}}', 'gi'), data[prop]);
                    }
                }
            }
            return html;
        },

        // EVENTS
        addEvent: function (target, type, callback, capture) {
            target.addEventListener(type, callback, !!capture);
        },


        // AJAX
        ajax: function () {
            var configs = {},
                url,
                callback,
                prop;
            switch (typeof arguments[0]) {
                case 'function':
                    callback = arguments[0];
                    break;
                case 'object':
                    // first parameter is configs object
                    // second callback
                    configs = arguments[0];
                    callback = arguments[1];
                    break;
                case 'string':
                    // first parameter is url string
                    // second callback
                    url = arguments[0];
                    callback = arguments[1];
                    break;
                default:
                    console.error('different params for ajax');
            }

            prop = {
                url: configs.url || url || this.url,
                method: configs.method || 'GET',
                async: (configs.async === false) ? false : true,
                data: configs.data || {}
            };

            this._ajaxRequest(prop, callback);
        },

        _ajaxRequest: function (prop, callback) {

            var xhr = new XMLHttpRequest();
            xhr.open(prop.method, prop.url, prop.async);
            xhr.send();

            if (!prop.async) {
                // SYNC AJAX
                callback(xhr.responseText);
            } else {
                // ASYNC AJAX
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 200 || xhr.status === 304) {
                            !callback || callback(xhr.responseText);
                        }
                    }
                };
            }
        }
    };

    window.L = window.L || {};
    window.L.lib = lib;
})(window);