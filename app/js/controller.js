(function (window) {
    'use strict';

    var Controller = function (model, view) {
        this.model = model;
        this.view = view;
    };

    /**
     * Start working with application
     */
    Controller.prototype.init = function () {
        // check if LS is empty
        if (this.model.getAllItemsNumber()) {
            this.view.renderList(this.model.getAllItems());
            this.view.updateCounter(this.model.getAllItemsNumber());
        }

        // add event to handle Adding new item to list
        this.view.addHandlers('addButton', 'click', this.addItem.bind(this));

        // add event to handle removing item from list
        L.lib.addEvent(L.lib.qs('body'), 'click', this.removeItem.bind(this));
    };

    Controller.prototype.addItem = function (evt) {
        var e = evt || event;
        e.preventDefault();
        // get title for item from text field
        var itemTitle = this.view.$addField.value.trim();
        if (itemTitle) {
            // add item to store and LS
            this.model.addItem(itemTitle);
            // add item to DOM
            this.view.addItem(itemTitle, this.model.getId());
            // update counter
            this.view.updateCounter(this.model.getAllItemsNumber());
        }
    };

    Controller.prototype.removeItem = function (e) {
        // EVENT DELEGATION
        // Event is added to the whole body
        // check if click was performed on delete button
        var target = e.target;
        var isDelegationCorrect = Array.prototype.some.call(target.classList, function (element, index, array) {
            return element === "remove-to-buy-item";
        });

        if (isDelegationCorrect) {
            var liEl = target.parentNode,
                itemId = liEl.dataset.id;
            // remove from DOM
            this.view.removeEl(liEl);
            // remove from store
            this.model.removeItem(itemId);
            // update counter
            this.view.updateCounter(this.model.getAllItemsNumber());
        }
    };


    window.L = window.L || {};
    window.L.Controller = Controller;
})(window);