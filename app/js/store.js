/* jshint expr:true */
(function (window) {
    'use strict';

    /**
     * Creation of storage for ToBuyList application (shortly 'L')
     * Trying to use browser's LocalStorage
     */
    var Store = function (name, callback) {
        var tempStorageName = 'toBuyList_' + new Date().getTime();

        this._storeName = name || tempStorageName;
        this._itemId;
        this.allItemsNumber;

        if(!localStorage[name]){
            // structure of storing data in LS
            var toBuyList = {
                items: []
            };
            localStorage.setItem(name, JSON.stringify(toBuyList));
        } else {
            this.allItemsNumber = this._getStore().items.length;
            // TODO update list from LS after refresh
        }
    };
    /**
     * Generate id for new item. Autoincrementing
     * @returns {number}
     */
    Store.prototype._generateIdForItem = function () {
        this._itemId = new Date().getTime();
        return this._itemId;
    };

    Store.prototype.getId = function () {
        return this._itemId;
    };

    /**
     * Add new item to store and save store to LS
     * @param title
     */
    Store.prototype.addItem = function (title) {
        var that = this,
            itemId = that._generateIdForItem(),
            // creating new item
            newItem = {
                id: itemId,
                title: title
            },

            // get store from LS
            store = that._getStore();

        // add new item to store.items array
        store.items.push(newItem);

        // save to LS
        this._saveStore(store);

        return itemId;
    };

    /**
     * TODO Add updating functionality and merge this method with 'addItem'
     * Update item
     * @param title
     * @param id
     */
    Store.prototype.updateItem = function (title, id) {

    };
    /**
     * Remove item and update LS
     * @param title
     */
    Store.prototype.removeItem = function (id) {
        var store = this._getStore(),
            itemIndex = this._findItem(id, store);
        //itemIndex ? store.items.splice(itemIndex, 1) : console.error('Error while removing\nCannot find item with id=' + id);
        store.items.splice(itemIndex, 1);
        // update store
        this._saveStore(store);
    };
    /**
     * Get saved string in browser's LS, parse it, and return as JS object
     * @returns {*} - object
     */
    Store.prototype._getStore = function () {
        return JSON.parse(localStorage[this._storeName]);
    };

    /**
     *
     * @param obj - JS object which contains all items for ToBuyList application
     */
    Store.prototype._saveStore = function (obj) {
        localStorage.setItem(this._storeName, JSON.stringify(obj));
        this._updateCouter();
    };

    Store.prototype._findItem = function (id, store) {
        var newStore = store || this._getStore(),
            itemIndex;
        newStore.items.forEach(function (val, index) {
            if (val.id == id) {
                itemIndex = index;
            }
        });
        return itemIndex;
    };

    Store.prototype._updateCouter = function (store) {
        var newStore = store || this._getStore();
        this.allItemsNumber = newStore.items.length;
    };

    Store.prototype.getAllItems = function () {
        var store = this._getStore();
        return store.items;
    };

    window.L = window.L || {};
    window.L.Store = Store;
})(window);