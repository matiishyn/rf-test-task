module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        jshint: {
            files: ['app/js/*.js'],
            options: {
                globals: {
                    console: true
                }
            }
        },

        watch: {
            files: ['<%= jshint.files %>'],
            tasks: []
        },

        jasmine: {
            pivotal: {
                src: [
                    'app/js/lib.js',
                    'app/js/store.js',
                    'app/js/model.js',
                    'app/js/template.js',
                    'app/js/view.js',
                    'app/js/controller.js'
                ],
                options: {
                    specs: 'tests/*Spec.js',
                    keepRunner: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-jasmine');

    grunt.registerTask('default', ['jshint']);
};