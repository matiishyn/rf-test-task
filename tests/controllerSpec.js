describe('controller', function () {
    'use strict';
    var model, view, store, template, controller, testItemTmpl,

        setUpObj = function () {
            store = new L.Store('TestApp');
            model = new L.Model(store);
            template = new L.Template();
            view = new L.View(template);
            controller = new L.Controller(model, view);
            // add html to body
            document.body.innerHTML += '<hr><hr><div class="wrapper"> <header> <h1>To Buy list</h1> </header> <section class="list-controllers"> <form action="#"> <input type="text" placeholder="Add item to list"/> <button>Add</button> </form> </section> <section class="to-buy-list"> </section> <footer> <div><em><span class="list-counter">0</span> items in list</em></div> </footer> </div>';
            testItemTmpl = '<li class="to-buy-item" data-id="{{id}}">{{title}} - <button class="remove-to-buy-item">Delete</button></li>';
        };

    beforeEach(function () {
        setUpObj();
        //jasmine.Ajax.install();
    });

    afterEach(function() {
        //jasmine.Ajax.uninstall();
    });

    it('should be created', function () {
        var controller = new L.Controller(model, view);
        expect(controller).toBeDefined();
    });

    it('should be initialized', function () {
        spyOn(model, 'getAllItemsNumber');
        controller.init();
        expect(model.getAllItemsNumber).toHaveBeenCalled();
    });

    it('should add new item to DOM list', function () {
        view.$addField.value = 'test item';
        // I had some troubles with loading ajax request so I skipped ajax
        // controller.addItem();
        var obj = {
            id: 1,
            title: 'test item'
        };
        var newItem = L.lib._replaceInTmpl(testItemTmpl, obj);
        view._renderItem(newItem);
        expect(view.$list.childElementCount).toBe(1);
    });

    it('should add new item to LocalStorage', function () {
        var itemTitle = 'test item';
        model.addItem(itemTitle);
        var itemLength = JSON.parse(localStorage.getItem('TestApp')).items.length;
        expect(itemLength).toBeGreaterThan(0);
    })
});